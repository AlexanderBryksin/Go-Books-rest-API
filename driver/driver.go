package driver

import (
	"database/sql"
	"fmt"
	"github.com/aleksk1ng/Go-Books-rest-API/tools"
	"github.com/lib/pq"
	"os"
)

var db *sql.DB

func ConnectDB() *sql.DB {
	tools.PanicRecover()

	pgUrl, err := pq.ParseURL(os.Getenv("ELEPHANTSQL_URL"))
	tools.LogFatal(err)

	// Init SQL DB
	db, err = sql.Open("postgres", pgUrl)
	tools.LogFatal(err)

	err = db.Ping()
	tools.LogFatal(err)
	fmt.Println("Database Connected")

	return db
}
