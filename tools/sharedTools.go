package tools

import (
	"fmt"
)

func PanicRecover() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("recovered", err)
		}
	}()
}

func LogFatal(err error) {
	if err != nil {
		//log.Fatal(err)
		panic(err.Error())
	}
}
