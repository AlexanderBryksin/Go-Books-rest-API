<p align="center">
  <a href="https://golang.org" target="blank"><img src="https://cdn-images-1.medium.com/max/1200/1*yh90bW8jL4f8pOTZTvbzqw.png" alt="Go Logo" width="320"/></a>
</p>

# Go Books Rest API




##### Technologies used:

* [gorilla/mux](https://github.com/gorilla/mux)
* [gotenv](https://github.com/subosito/gotenv)
* [ElephantSQL](https://www.elephantsql.com/) - PostgreSQL
* [pq](https://github.com/lib/pq) - pure Go postgres driver for Go's database/sql package








##### Author [Alexander Bryksin](https://github.com/AleksK1NG)
