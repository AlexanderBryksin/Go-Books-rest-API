package bookRepository

import (
	"database/sql"
	"github.com/aleksk1ng/Go-Books-rest-API/models"
	"github.com/aleksk1ng/Go-Books-rest-API/tools"
)

type BookRepository struct{}

// Handlers

func (b BookRepository) GetBooks(db *sql.DB, book models.Book, books []models.Book) []models.Book {
	rows, err := db.Query("select * from books")
	tools.LogFatal(err)

	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&book.ID, &book.Title, &book.Author, &book.Year)
		tools.LogFatal(err)

		books = append(books, book)
	}

	return books
}

func (b BookRepository) GetBook(db *sql.DB, book models.Book, id int) models.Book {
	rows := db.QueryRow("select * from books where id=$1", id)

	err := rows.Scan(&book.ID, &book.Title, &book.Author, &book.Year)
	tools.LogFatal(err)

	return book
}

func (b BookRepository) AddBook(db *sql.DB, book models.Book) int {

	err := db.QueryRow("insert into books (title, author, year) values($1, $2, $3) RETURNING id;",
		book.Title, book.Author, book.Year).Scan(&book.ID)

	tools.LogFatal(err)

	return book.ID
}

func (b BookRepository) UpdateBook(db *sql.DB, book models.Book, id int) int64 {

	result, err := db.Exec("update books set title=$1, author=$2, year=$3 where id=$4 RETURNING id",
		&book.Title, &book.Author, &book.Year, id)

	tools.LogFatal(err)

	rowsUpdated, err := result.RowsAffected()
	tools.LogFatal(err)

	return rowsUpdated
}

func (b BookRepository) DeleteBook(db *sql.DB, id int) int64 {

	result, err := db.Exec("delete from books where id = $1", id)
	tools.LogFatal(err)

	rowsDeleted, err := result.RowsAffected()
	tools.LogFatal(err)

	return rowsDeleted
}
