package controllers

import (
	"database/sql"
	"encoding/json"
	"github.com/aleksk1ng/Go-Books-rest-API/models"
	"github.com/aleksk1ng/Go-Books-rest-API/repository/book"
	"github.com/aleksk1ng/Go-Books-rest-API/tools"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

var books []models.Book

type Controller struct{}

func (c Controller) GetBooks(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		var book models.Book
		books = []models.Book{}
		bookRepo := bookRepository.BookRepository{}

		books = bookRepo.GetBooks(db, book, books)

		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(books)
	}
}

func (c Controller) GetBook(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		params := mux.Vars(r)
		var book models.Book
		bookRepo := bookRepository.BookRepository{}

		id, err := strconv.Atoi(params["id"])
		tools.LogFatal(err)
		book = bookRepo.GetBook(db, book, id)

		json.NewEncoder(w).Encode(&book)
	}
}

func (c Controller) AddBook(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		var book models.Book
		var bookID int
		json.NewDecoder(r.Body).Decode(&book)

		bookRepo := bookRepository.BookRepository{}
		bookID = bookRepo.AddBook(db, book)

		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(bookID)
	}
}

func (c Controller) UpdateBook(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		params := mux.Vars(r)
		var book models.Book
		json.NewDecoder(r.Body).Decode(&book)

		bookRepo := bookRepository.BookRepository{}

		id, err := strconv.Atoi(params["id"])
		tools.LogFatal(err)
		rowsUpdated := bookRepo.UpdateBook(db, book, id)

		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(rowsUpdated)
	}
}

func (c Controller) DeleteBook(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		params := mux.Vars(r)
		bookRepo := bookRepository.BookRepository{}

		id, err := strconv.Atoi(params["id"])
		tools.LogFatal(err)
		rowsUpdated := bookRepo.DeleteBook(db, id)

		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(rowsUpdated)

	}
}
