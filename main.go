package main

import (
	"database/sql"
	"fmt"
	"github.com/aleksk1ng/Go-Books-rest-API/controllers"
	"github.com/aleksk1ng/Go-Books-rest-API/driver"
	"github.com/aleksk1ng/Go-Books-rest-API/middlewares"
	"github.com/aleksk1ng/Go-Books-rest-API/models"
	"github.com/aleksk1ng/Go-Books-rest-API/tools"
	"github.com/gorilla/mux"
	"github.com/subosito/gotenv"
	"log"
	"net/http"
)

var books []models.Book
// SQL Database
var db *sql.DB

func init() {
	err := gotenv.Load()
	tools.LogFatal(err)
}

func main() {
	tools.PanicRecover()

	// Connect DB
	db = driver.ConnectDB()
	booksController := controllers.Controller{}
	// Init Router
	r := mux.NewRouter()

	// Middlewares
	r.Use(middlewares.PanicMiddleware)

	// Router handlers
	r.HandleFunc("/books", booksController.GetBooks(db)).Methods("GET")
	r.HandleFunc("/books/{id}", booksController.GetBook(db)).Methods("GET")
	r.HandleFunc("/books", booksController.AddBook(db)).Methods("POST")
	r.HandleFunc("/books/{id}", booksController.UpdateBook(db)).Methods("PUT")
	r.HandleFunc("/books/{id}", booksController.DeleteBook(db)).Methods("DELETE")

	fmt.Println("Server lictetning on port :8000")
	log.Fatal(http.ListenAndServe(":8000", r))
}
